package com.mycompany.homeWork31;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.testng.annotations.Test;

public class HomeTest31 {

    @Test

    public void runBrowserChrome() {
        System.setProperty("webdriver.chrome.driver", "C:/tools/browser_drivers/chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://t-vac-testschool:38443/");

        driver.quit();
    }

    @Test
    public void runBrowserFirefox() {
        System.setProperty("webdriver.gecko.driver", "C:/tools/browser_drivers/geckodriver.exe");

        WebDriver driver = new FirefoxDriver();

        driver.get("https://t-vac-testschool:38443/");

        driver.quit();
    }
}
